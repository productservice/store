package main

import (
	"github/FIrstService/template-service/product-service/store/config"
	pb "github/FIrstService/template-service/product-service/store/genproto/stores"
	"github/FIrstService/template-service/product-service/store/pkg/db"
	"github/FIrstService/template-service/product-service/store/pkg/logger"
	"github/FIrstService/template-service/product-service/store/service"
	"net"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {
	cfg := config.Load()

	log := logger.New(cfg.LogLevel, "/product-service/Store")
	defer logger.Cleanup(log)

	log.Info("main: sqlxConfig",
		logger.String("host", cfg.PostgresHost),
		logger.Int("port", cfg.PostgresPort),
		logger.String("database", cfg.PostgresDatabase))

	connDB, err := db.ConnectTODB(cfg)
	if err != nil {
		log.Fatal("sqlx connection to postgres error", logger.Error(err))
	}

	storeService := service.NewStoreService(connDB, log)

	lis, err := net.Listen("tcp", cfg.RPCPort)
	if err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}

	s := grpc.NewServer()
	reflection.Register(s)
	pb.RegisterStoreServiceServer(s, storeService)
	log.Info("main: server runnig",
		logger.String("port", cfg.RPCPort))
	if err := s.Serve(lis); err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}

}
