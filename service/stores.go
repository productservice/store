package service

import (
	"context"
	"fmt"
	pb "github/FIrstService/template-service/product-service/store/genproto/stores"
	l "github/FIrstService/template-service/product-service/store/pkg/logger"
	"github/FIrstService/template-service/product-service/store/storage"

	"github.com/jmoiron/sqlx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// StoreService ...

type StoreService struct {
	storage storage.IStorage
	logger  l.Logger
}

// NewStoreService ...

func NewStoreService(db *sqlx.DB, log l.Logger) *StoreService {
	return &StoreService{
		storage: storage.NewStoragePg(db),
		logger:  log,
	}
}

func (s *StoreService) CreateStore(ctx context.Context, req *pb.StoreRequest) (*pb.StoreResponse, error) {
	store, err := s.storage.Store().CreateStore(req)
	if err != nil {
		s.logger.Error("Error while insert", l.Any("Error insert stores", err))
		return &pb.StoreResponse{}, status.Error(codes.Internal, "something went wrong,please check store info")
	}
	fmt.Println(store, err)
	return store, nil
}

// func (s *StoreService) CreateAddress(ctx context.Context, req *pb.Address) (*pb.AddressResp, error) {
// 	q, err := s.storage.Store().CreateAddress(req)
// 	if err != nil {
// 		s.logger.Error("Error while insert", l.Any("Error insert addresses", err))
// 		return &pb.AddressResp{}, status.Error(codes.Internal, "something went wrong,please check store infto")
// 	}
// 	return q, nil
// }

func (s *StoreService) GetStore(ctx context.Context, req *pb.GetstoreInfobyid) (*pb.StoreResponse, error) {
	q, err := s.storage.Store().GetStore(req)
	if err != nil {
		s.logger.Error("Error while select", l.Any("Error select stores", err))
		return &pb.StoreResponse{}, status.Error(codes.Internal, "something went wrong,please check store id infto")
	}
	return q, nil
}

// func (s *ProductService) CreateType(ctx context.Context,req *pb.Type) (*pb.Type,error) {
// 	q, err := s.storage.Product().CreateType(req)
// 	if err != nil {
// 		s.logger.Error("Error while insert", l.Any("Error insert product", err))
// 		return &pb.Type{}, status.Error(codes.Internal, "something went wrong,please check product infto")
// 	}
// 	return q, nil
// }

// func (s *ProductService) GetProductInfoByid(ctx context.Context, req *pb.Ids) (*pb.GetProducts, error) {
// 	res, err := s.storage.Product().GetProductInfoByid(req)
// 	if err != nil {
// 		s.logger.Error("Error while get info", l.Any("Error select product", err))
// 		return &pb.GetProducts{}, status.Error(codes.Internal, "something went wrong,please check product info")
// 	}
// 	return res, err
// }

// func (s *ProductService) UpdateByid(ctx context.Context, req *pb.Product) (*pb.Product, error) {
// 	res, err := s.storage.Product().UpdateByid(req)
// 	if err != nil {
// 		s.logger.Error("Error while updating", l.Any("Update", err))
// 		return &pb.Product{}, status.Error(codes.InvalidArgument, "Please recheck user info")
// 	}
// 	return res, nil

// }

// func (s *ProductService) DeleteInfo(ctx context.Context, req *pb.Ids) (*pb.Empty, error) {
// 	err := s.storage.Product().DeleteInfo(req)
// 	if err != nil {
// 		s.logger.Error("Error while delete product", l.Any("Delete", err))
// 		return &pb.Empty{}, status.Error(codes.Internal, "wrong id for delete")
// 	}
// 	return &pb.Empty{}, nil
// }
