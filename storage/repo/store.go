package repo

import (
	pb "github/FIrstService/template-service/product-service/store/genproto/stores"
)

// StoreStorageI ...
type StoreStorageI interface {
	CreateStore(*pb.StoreRequest) (*pb.StoreResponse, error)
	// CreateAddress(*pb.Address) (*pb.AddressResp, error)
	GetStore(*pb.GetstoreInfobyid) (*pb.StoreResponse, error)
	// CreateStoresProduct(*pb.StoresProduct) (*pb.Empty, error)
	// GetProductInfoByid(*pb.Ids) (*pb.GetProducts, error)
	// DeleteInfo(*pb.Ids) error
	// UpdateByid(*pb.Product) (*pb.Product, error)
}
