package postgres

import (
	"fmt"
	pb "github/FIrstService/template-service/product-service/store/genproto/stores"

	"github.com/jmoiron/sqlx"
)

type storeRepo struct {
	db *sqlx.DB
}

// NewStoreRepo ...

func NewStoreRepo(db *sqlx.DB) *storeRepo {
	return &storeRepo{db: db}
}

func (r *storeRepo) CreateStore(store *pb.StoreRequest) (*pb.StoreResponse, error) {
	storeResp := pb.StoreResponse{}
	err := r.db.QueryRow(`insert into stores (name) values ($1) returning id,name`, store.Name).Scan(&storeResp.Id, &storeResp.Name)
	fmt.Println(storeResp, err)
	if err != nil {
		return &pb.StoreResponse{}, err
	}
	addresses := []*pb.AddressResp{}
	for _, address := range store.Address {
		addressResp := pb.AddressResp{}
		err = r.db.QueryRow(`
		insert into addresses(district,street) values($1,$2)
		returning id,district,street`, address.District, address.Street).Scan(
			&addressResp.Id, &addressResp.District, &addressResp.Street,
		)
		_, err = r.db.Exec(`
		insert into store_addresses (store_id,address_id)
		values($1,$2)`, storeResp.Id, addressResp.Id)
		if err != nil {
			return &pb.StoreResponse{}, err
		}
		addresses = append(addresses, &addressResp)
	}
	storeResp.Address = addresses
	return &storeResp, nil
}

func (r *storeRepo) GetStore(req *pb.GetstoreInfobyid) (*pb.StoreResponse, error) {
	store := pb.StoreResponse{}
	err := r.db.QueryRow(`select * from stores where id=$1`, req.Id).Scan(&store.Id, &store.Name)
	if err != nil {
		fmt.Println(err)
		return &pb.StoreResponse{}, err
	}
	rows, err := r.db.Query(`
	select a.id,a.district,a.street
	from addresses a inner join store_addresses sa
	on sa.address_id=a.id and sa.store_id=$1
	`, store.Id)
	addresses := []*pb.AddressResp{}
	for rows.Next() {
		address := pb.AddressResp{}
		err = rows.Scan(&address.Id, &address.District, &address.Street)
		if err != nil {
			return &pb.StoreResponse{}, err
		}
		addresses = append(addresses, &address)
	}

	store.Address = addresses
	return &store, nil
}

// func (r *productRepo) GetProductInfoByid(ids *pb.Ids) (*pb.GetProducts, error) {
// 	fmt.Println(ids)
// 	response := &pb.GetProducts{}
// 	for _, id := range ids.Id {
// 		tempUser := &pb.Product{}
// 		err := r.db.QueryRow(`select * from products where id=$1`, id).Scan(&tempUser.Id, &tempUser.Name, &tempUser.Model, &tempUser.TypeId, &tempUser.CategoryId, &tempUser.Price, &tempUser.Amount)
// 		if err != nil {
// 			log.Fatal("Error while select products", err)
// 		}
// 		response.Products = append(response.Products, tempUser)
// 	}
// 	return response, nil
// }

// func (r *productRepo) UpdateByid(req *pb.Product) (*pb.Product, error) {
// 	_, err := r.db.Exec(`UPDATE products SET name=$1, model=$2 where id=$3`,
// 		req.Name, req.Model, req.Id)
// 	fmt.Println(err)
// 	return req, err
// }

// func (r *productRepo) DeleteInfo(ids *pb.Ids) error {
// 	for _, id := range ids.Id {
// 		_, err := r.db.Exec(`DELETE FROM products WHERE id=$1`, id)
// 		if err != nil {
// 			log.Fatal("Error while delete product", err)
// 		}
// 	}
// 	return nil
// }
