package storage

import (
	"github/FIrstService/template-service/product-service/store/storage/postgres"
	"github/FIrstService/template-service/product-service/store/storage/repo"

	"github.com/jmoiron/sqlx"
)

type IStorage interface {
	Store() repo.StoreStorageI
}

type storagePg struct {
	db        *sqlx.DB
	storeRepo repo.StoreStorageI
}

func NewStoragePg(db *sqlx.DB) *storagePg {
	return &storagePg{
		db:        db,
		storeRepo: postgres.NewStoreRepo(db),
	}
}

func (s storagePg) Store() repo.StoreStorageI {
	return s.storeRepo
}
