CREATE Table if not exists store_addresses(
  store_id int REFERENCES stores(id),
  address_id int REFERENCES addresses(id)
);